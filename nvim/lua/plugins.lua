local Plug = vim.fn['plug#']

vim.call('plug#begin', '~/.config/nvim/plugged')
-- Color scheme
Plug('catppuccin/nvim', {as='catppuccin'})


-- Telescope 
Plug('nvim-lua/plenary.nvim')
Plug('nvim-telescope/telescope.nvim')

-- Treesitter
Plug('nvim-treesitter/nvim-treesitter', {['do'] = ':TSUpdate'})

-- Lualine and icons
Plug('nvim-lualine/lualine.nvim')
Plug('kyazdani42/nvim-web-devicons')
Plug('ryanoasis/vim-devicons')

-- buffer indicator
Plug('akinsho/bufferline.nvim')

-- Tree
Plug('kyazdani42/nvim-tree.lua')

-- Autotag
Plug('windwp/nvim-ts-autotag')

-- Autopairs
Plug('windwp/nvim-autopairs')

-- Auto completion
Plug('neovim/nvim-lspconfig')
Plug('hrsh7th/cmp-nvim-lsp')
Plug('hrsh7th/cmp-buffer')
Plug('hrsh7th/cmp-path')
Plug('hrsh7th/cmp-cmdline')
Plug('hrsh7th/nvim-cmp')

Plug('hrsh7th/cmp-vsnip')
Plug('hrsh7th/vim-vsnip')
Plug('hrsh7th/vim-vsnip-integ')

-- This tiny plugin adds vscode-like pictograms to neovim built-in lsp:
Plug('onsails/lspkind-nvim')


vim.call('plug#end')
